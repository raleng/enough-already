package com.ralfengbers.enoughalready

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.AudioManager
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.Toast

class EACallReceiver : BroadcastReceiver() {

    private lateinit var sharedPrefs: SharedPreferences

    override fun onReceive(context: Context, intent: Intent) {

        sharedPrefs =
            context.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE)

        Log.d(TAG, "onReceive with intent: ${intent.action}")
        if ("android.intent.action.PHONE_STATE" == intent.action
            && sharedPrefs.getBoolean(Constants.APP_IS_ACTIVE, false)
        ) {
            if (intent.getStringExtra(TelephonyManager.EXTRA_STATE) == TelephonyManager.EXTRA_STATE_RINGING) {
                Toast.makeText(context, "call!!", Toast.LENGTH_LONG).show()
                val cachedRingerMode = sharedPrefs.getInt(Constants.CACHED_RINGERMODE, -1)
                val myAudioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
                myAudioManager.ringerMode = AudioManager.RINGER_MODE_VIBRATE
                val v = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                val pattern = longArrayOf(0, 200, 0)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    VibrationEffect.createWaveform(pattern, 2)
                } else {
                    v.vibrate(pattern, 2)
                }
            }
        }
    }

    companion object {
        const val TAG: String = "EA|CallReceiver"
    }
}