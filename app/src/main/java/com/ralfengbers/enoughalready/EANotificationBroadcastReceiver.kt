package com.ralfengbers.enoughalready

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.AudioManager
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.core.content.ContextCompat

class EANotificationBroadcastReceiver : BroadcastReceiver(), AsyncResponse {
    private lateinit var sharedPrefs: SharedPreferences

    private var myAudioManager: AudioManager? = null
    private var myNotificationManager: NotificationManager? = null
    private lateinit var myContext: Context
    private lateinit var myIntent: Intent

    private var cachedRingerMode: Int? = null
    private lateinit var currentPackage: Package

    override fun onReceive(context: Context, intent: Intent) {
        Log.d(TAG, "onReceive: $intent")

        myAudioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        myNotificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        myContext = context
        myIntent = intent

        // get the sharedPreferences to access cached ringerMode
        sharedPrefs = context.getSharedPreferences(
            context.getString(R.string.app_settings_file_key), Context.MODE_PRIVATE
        )

        // get the intent extras (aside from intent action, only packageName)
        val extras: Bundle = intent.extras!!
        val packageName = extras.getString("packageName")

        // package lookup in database needs to be async
        // since everything else depends on the package lookup, everything is in here as well
        if (packageName != context.resources.getString(R.string.package_name)) {
            PackageDatabase.getInstance(context.applicationContext).packageDao()
                .run { StartServiceAsyncTask(this) }
                .run {
                    delegate = this@EANotificationBroadcastReceiver
                    Log.d(TAG, "onReceive | Start Async: ${delegate.toString()}")
                    execute(packageName)
                }
        }
    }

    override fun processFinish(output: Package) {
        Log.d(TAG, "processFinish: $output")


        myNotificationManager?.activeNotifications?.forEach {
            Log.d(
                TAG,
                "Active Notifications: $it | ${it.isClearable} | ${it.isOngoing} | ${it.postTime} | ${it.tag}"
            )

        }
        currentPackage = output
        // check if the app sending the notification is isActivated and
        // whether a notification is received or removed
        if (currentPackage.isActivated) {
            when {
                myIntent.action == Constants.NOTIFICATION_RECEIVED && currentPackage.timer == 0
                -> notificationReceived(myContext)
                myIntent.action == Constants.NOTIFICATION_REMOVED && currentPackage.timer > 0L
                -> notificationRemoved(myContext)
            }
        }
        // if there are actively muted apps, push an extra notification for non-active apps
        // idea: activeNotifications should contain the pinned mute notification
        else if (myIntent.action == Constants.NOTIFICATION_RECEIVED
            && myNotificationManager?.activeNotifications!!.isNotEmpty()
        ) {
            Log.d(TAG, "processFinish | Active test: ${currentPackage.packageName}")
            Intent(myContext, EAMutePhoneService::class.java).run {
                action = Constants.SEND_NOTIFICATION
                putExtra("currentPackage", currentPackage)
                putExtra(
                    Constants.CACHED_RINGERMODE,
                    sharedPrefs.getInt(myContext.getString(R.string.ringermode_cache), -1)
                )
                ContextCompat.startForegroundService(myContext, this)
            }
        }
    }

    private class StartServiceAsyncTask(private val dao: PackageDao) :
        AsyncTask<String, Void, Package>() {

        var delegate: AsyncResponse? = null

        override fun doInBackground(vararg packageName: String): Package {
            Log.d(TAG, "doInBackground: $packageName")
            return dao.getPackage(packageName[0])
        }

        override fun onPostExecute(currentPackage: Package?) {
            Log.d(TAG, "onPostExecute: ${currentPackage?.packageName}, $delegate")
            currentPackage?.let { delegate?.processFinish(it) }
        }
    }

    private fun notificationReceived(context: Context) {
        Log.d(
            TAG,
            "notificationReceived | ${currentPackage.packageName} timer: ${currentPackage.timer}"
        )
        // if the mute service is not running, cache current ringerMode
        // else get the cached ringerMode from sharedPrefs

        if (myNotificationManager?.activeNotifications!!.isEmpty()) {
            cachedRingerMode = myAudioManager?.ringerMode
            with(sharedPrefs.edit()) {
                putInt(
                    context.resources.getString(R.string.ringermode_cache),
                    cachedRingerMode!!
                )
                apply()
            }
        } else cachedRingerMode =
            sharedPrefs.getInt(context.getString(R.string.ringermode_cache), -1)

        // create new mute intent with cached ringerMode and packageName
        Intent(context, EAMutePhoneService::class.java).run {
            action = Constants.START_FOREGROUND_SERVICE_ACTION
            putExtra(Constants.CACHED_RINGERMODE, cachedRingerMode)
            putExtra("currentPackage", currentPackage)
            ContextCompat.startForegroundService(context, this)
        }

        // start foreground service
        Log.d(TAG, "notificationReceived | start foreground service")

    }

    private fun notificationRemoved(context: Context) {
        Log.d(TAG, "notificationRemoved")
        // if the package has an active timer, send a stop service intent
        Log.d(TAG, "notificationRemoved | timer: ${currentPackage.timer}")

        Intent(context, EAMutePhoneService::class.java).run {
            action = Constants.STOP_FOREGROUND_SERVICE_ACTION
            putExtra("currentPackage", currentPackage)
            ContextCompat.startForegroundService(context, this)
        }
        Log.d(TAG, "notificationReceived | stop foreground service")
    }

    companion object {
        const val TAG: String = "EA|BroadcastReceiver"
    }
}

interface AsyncResponse {
    fun processFinish(output: Package)
}
