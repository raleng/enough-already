package com.ralfengbers.enoughalready

object Constants {
    const val START_FOREGROUND_SERVICE_ACTION = "com.ralfengbers.enoughalready.START_SERVICE"
    const val STOP_FOREGROUND_SERVICE_ACTION = "com.ralfengbers.enoughalready.STOP_SERVICE"
    const val SEND_NOTIFICATION = "com.ralfengbers.enoughalready.SEND_NOTIFICATIONt"
    const val NOTIFICATION_RECEIVED = "com.ralfengbers.enoughalready.NOTIFICATION_RECEIVED"
    const val NOTIFICATION_REMOVED = "com.ralfengbers.enoughalready.NOTIFICATION_REMOVED"
    const val SHARED_PREFERENCES = "shared_preferences"
    const val APP_IS_ACTIVE = "app_active_state"
    const val APP_FILTER_IS_ACTIVE = "app_filter_active_state"
    const val CACHED_RINGERMODE = "cached_ringer_mode"
    const val MUTE_TIME_DEFAULT = 300_000
}