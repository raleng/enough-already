package com.ralfengbers.enoughalready

import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.os.AsyncTask
import android.os.CountDownTimer
import android.os.IBinder
import android.os.Handler
import android.util.Log
import androidx.core.app.NotificationCompat
import com.ralfengbers.enoughalready.App.Companion.CHANNEL_ID_FOREGROUND

/*
EAMute Phone Service

- is started by the EANotificationBroadcastReceiver
- starts a timer according to the setting for the specific app
- stops itself after all timers are finished
 */
class EAMutePhoneService : Service() {

    private var myAudioManager: AudioManager? = null
    private var cachedRingerMode: Int = -1

    private val listOfTimers = mutableMapOf<String, CountDownTimer>()

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand")
        myAudioManager = this.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        cachedRingerMode = intent.getIntExtra(Constants.CACHED_RINGERMODE, -1)

        val currentPackage = intent.getParcelableExtra<Package>("currentPackage")
        Log.d(TAG, "onStartCommand | getParcelableExtra: $currentPackage")

        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)

        when (intent.action) {
            Constants.START_FOREGROUND_SERVICE_ACTION -> startMuting(currentPackage)
            Constants.STOP_FOREGROUND_SERVICE_ACTION -> stopMuting(currentPackage)
            Constants.SEND_NOTIFICATION -> sendNotification(this)
        }

        NotificationCompat.Builder(this, CHANNEL_ID_FOREGROUND)
            .setContentTitle("Enough Already Mute Service")
            .setContentText("App Name")
            .setSmallIcon(R.drawable.ic_timer)
            .setContentIntent(pendingIntent)
            .build()
            .run {
                Log.d(TAG, "onStartCommand | startForeground")
                startForeground(1, this)
            }

        //return super.onStartCommand(intent, flags, startId)

        //return super.onStartCommand(intent, flags, startId)
        return START_NOT_STICKY
    }

    private fun startMuting(currentPackage: Package) {
        Log.d(TAG, "onStartCommand | startMuting foreground service")
        currentPackage.run {
            timer = this.muteTime
            update(this)
        }
        if (listOfTimers.isEmpty()) myAudioManager?.ringerMode = AudioManager.RINGER_MODE_SILENT

        // TODO: Default MuteTime should be global app mute time
        UnmuteTimer(currentPackage, cachedRingerMode).run {
            listOfTimers[currentPackage.packageName] = this
            start()
        }.also {
            Log.d(TAG, "onStartCommand | TimerList Size (after startMuting): ${listOfTimers.size}")
        }
    }

    private fun stopMuting(currentPackage: Package) {
        Log.d(TAG, "onStartCommand | stopMuting foreground service")
        currentPackage.run {
            listOfTimers[this.packageName]?.let {
                it.cancel()
                it.onFinish()
            }
            listOfTimers.remove(this.packageName)
            timer = 0
            update(this)
        }
        Log.d(TAG, "onStartCommand | TimerList Size (after stopMuting): ${listOfTimers.size}")
        if (listOfTimers.isEmpty()) stopSelf()
    }

    private fun sendNotification(context: Context) {
        Log.d(TAG, "SENDNOTIFICATION: $cachedRingerMode | $myAudioManager")
        myAudioManager?.ringerMode = cachedRingerMode

        Log.d(TAG, "sendNotification")
        val notificationID = 101
        val channelID = context.resources.getString(R.string.notification_channel)
        val notification = NotificationCompat.Builder(context, channelID)
            .setContentTitle("Simulated Title")
            .setContentText("This is the simulated text of a simulated notification.")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setSmallIcon(android.R.drawable.ic_dialog_info)
            .setChannelId(channelID)
            .build()


        val myNotificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        myNotificationManager.run {
            notify(notificationID, notification)
            Handler().postDelayed({ this.cancel(notificationID) }, 5000)
        }
        // anonymous timer to reset silent mode after the notification has been posted
        object : CountDownTimer(1000, 100) {
            override fun onFinish() {
                Log.d(TAG, "processFinish | CountDownTimer.onFinish")
                myAudioManager?.ringerMode = AudioManager.RINGER_MODE_SILENT
            }

            override fun onTick(millisUntilFinished: Long) {}
        }.start()
        // TODO: Length also makes sense in global settings, in case someone uses long notification sounds
    }

    private fun update(currentPackage: Package) {
        Log.d(TAG, "update: $currentPackage")
        // get a database instance and a DAO
        PackageDatabase.getInstance(this.applicationContext).packageDao()
            .run { UpdatePackageAsyncTask(this) }
            .run { execute(currentPackage) }
            .also { Log.d(TAG, "update | AsyncTask: $currentPackage") }
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy")
        myAudioManager?.ringerMode = cachedRingerMode
        super.onDestroy()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private class UpdatePackageAsyncTask(private val dao: PackageDao) :
        AsyncTask<Package, Void, Void>() {
        override fun doInBackground(vararg pkg: Package): Void? {
            Log.d(TAG, "UpdatePackageAsyncTask | doInBackground: ${pkg[0]}")
            dao.update(pkg[0])
            return null
        }
    }

    inner class UnmuteTimer(private val pkg: Package, private val mode: Int) :
        CountDownTimer(pkg.muteTime.toLong(), TIMER_STEP) {

        override fun onFinish() {
            Log.d(TAG, "UnmuteTimer | onFinish: ${pkg.packageName}, listOfTimers: $listOfTimers")
            // update package
            pkg.run {
                timer = 0
                update(this)
                listOfTimers.remove(this.packageName)
            }
            Log.d(TAG, "UnmuteTimer | onFinish listOfTimers: $listOfTimers")
            // if there are no isActivated timers, reset the ringerMode
            if (listOfTimers.isEmpty()) {
                myAudioManager?.ringerMode = mode
                stopSelf()
            }
        }

        override fun onTick(millisUntilFinished: Long) {
            Log.d(TAG, "UnmuteTimer | onTick: $millisUntilFinished")
            pkg.run {
                timer = millisUntilFinished.toInt()
                update(this)
            }
        }
    }

    companion object {
        const val TAG: String = "EA|MutePhoneService"
        const val TIMER_STEP: Long = 1_000
    }
}