package com.ralfengbers.enoughalready

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.util.Log

class EANotificationListenerService : NotificationListenerService() {

    override fun onNotificationPosted(sbn: StatusBarNotification?) {
        val sharedPrefs: SharedPreferences = this.getSharedPreferences(
            Constants.SHARED_PREFERENCES,
            Context.MODE_PRIVATE
        )
        if (sharedPrefs.getBoolean(Constants.APP_IS_ACTIVE, false)) {
            sbn?.let {
                Log.d(TAG, "Notification posted: ${it.packageName}")
                Log.d("EXTRAS", it.notification.extras.toString())
                // don't listen to constant notifications
                if (it.isClearable) {
                    Intent(Constants.NOTIFICATION_RECEIVED).run {
                        setPackage(getString(R.string.package_name))
                        putExtra("title", it.notification.extras.getString("android.title"))
                        putExtra("packageName", it.packageName)
                        sendBroadcast(this)
                    }
                }
            }
        }
    }

    override fun onNotificationRemoved(sbn: StatusBarNotification?) {
        val sharedPrefs: SharedPreferences = this.getSharedPreferences(
            Constants.SHARED_PREFERENCES,
            Context.MODE_PRIVATE
        )
        if (sharedPrefs.getBoolean(Constants.APP_IS_ACTIVE, false))
            sbn?.let {
                Log.d(TAG, "Notification removed: ${it.packageName}")
                Intent(Constants.NOTIFICATION_REMOVED).run {
                    setPackage(getString(R.string.package_name))
                    putExtra("title", it.notification.extras.getString("android.title"))
                    putExtra("packageName", it.packageName)
                    sendBroadcast(this)
                }
            }
    }

    companion object {
        const val TAG: String = "EA|NotificationListener"
    }
}