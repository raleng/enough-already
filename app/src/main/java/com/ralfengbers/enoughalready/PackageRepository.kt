package com.ralfengbers.enoughalready

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData

class PackageRepository(private val application: Application) {

    // database and DAO
    private val database = PackageDatabase.getInstance(application)
    private val packageDao: PackageDao = database.packageDao()

    // LiveData
    val allPackages: LiveData<List<Package>> = packageDao.getAllPackages()
    val activePackages: LiveData<List<Package>> = packageDao.getActivePackages()
    val numberOfTimers: LiveData<Int> = packageDao.getNumberOfTimers()

    // public methods
    fun insert(pkg: Package) {
        if (pkg.packageName != this.application.getString(R.string.package_name)) {
            InsertPackageAsyncTask(packageDao).execute(pkg)
        }
    }

    fun update(pkg: Package) {
        UpdatePackageAsyncTask(packageDao).execute(pkg)
    }

    // AsyncTasks
    private class InsertPackageAsyncTask(private val packageDao: PackageDao) :
        AsyncTask<Package, Void, Void>() {

        override fun doInBackground(vararg pkg: Package): Void? {
            packageDao.insert(pkg[0])
            return null
        }
    }

    private class UpdatePackageAsyncTask(private val packageDao: PackageDao) :
        AsyncTask<Package, Void, Void>() {

        override fun doInBackground(vararg pkgs: Package): Void? {
            packageDao.update(pkgs[0])
            return null
        }
    }

}
