package com.ralfengbers.enoughalready

import android.app.Application
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData


class PackageViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: PackageRepository = PackageRepository(application)

    val allPackages: LiveData<List<Package>> = repository.allPackages
    val activePackages: LiveData<List<Package>> = repository.activePackages
    val numberOfTimers: LiveData<Int> = repository.numberOfTimers

    var ringerModeCache: Int? = null

    /*
    update single package
     */
    fun update(pkg: Package) {
        Log.d(TAG, "update")
        repository.update(pkg)
    }

    /*
    update installed packages
     */
    fun updateInstalledPackages(pm: PackageManager) {
        Log.d(TAG, "updateInstalledPackages")
        val installedPackages = pm.getInstalledPackages(0)

        val listOfPackages = mutableListOf<Package>()
        Log.d(TAG, "updateInstalledPackages | allPackages null: ${allPackages.value == null}")
        val stillInstalled =
            allPackages.value?.filter { pkg -> installedPackages.any { it.packageName == pkg.packageName } }

        stillInstalled?.let { listOfPackages.addAll(it) }

        Log.d(TAG, "updateInstalledPackages | Still installed size: ${stillInstalled?.size}")
        Log.d(TAG, "updateInstalledPackages | Installed size: ${installedPackages.size}")

        installedPackages
            .filter { pkg ->
                // applicationInfo.flags contains all flags
                // FLAG_SYSTEM == 1 indicates system image app
                pkg.applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM == 0
                        && listOfPackages.none { it.packageName == pkg.packageName }
            }
            .forEach {
                Package(it.packageName, it.applicationInfo.loadLabel(pm).toString()).run {
                    Log.d("ViewModel: ", "Added: ${this.packageName}")
                    repository.insert(this)
                }
            }
    }

    companion object {
        const val TAG: String = "EA|PkgViewModel"
    }
}