package com.ralfengbers.enoughalready

import android.app.Application
import android.app.NotificationManager
import android.app.NotificationChannel
import android.os.Build


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        createNotificationChannels()
    }

    private fun createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val foregroundChannel = NotificationChannel(
                CHANNEL_ID_FOREGROUND,
                "Foreground Service Notification Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )

            val simChannel = NotificationChannel(
                CHANNEL_ID_SIM,
                "Simulated Notification Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )

            getSystemService(NotificationManager::class.java).apply {
                createNotificationChannels(listOf(foregroundChannel, simChannel))
            }
        }
    }

    companion object {
        const val CHANNEL_ID_FOREGROUND = "foregroundNotificationChannel"
        const val CHANNEL_ID_SIM = "simulatedNotificationChannel"
    }
}