package com.ralfengbers.enoughalready

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface PackageDao {

    @Insert
    fun insert(pkg: Package)

    @Update
    fun update(pkg: Package)

    @Query("DELETE FROM package_table")
    fun deleteAllPackages()

    @Query("SELECT * FROM package_table WHERE packageName = :pkgName")
    fun getPackage(pkgName: String): Package

    @Query("SELECT * FROM package_table ORDER BY appName COLLATE NOCASE")
    fun getAllPackages(): LiveData<List<Package>>

    @Query("SELECT * FROM package_table WHERE isActivated = 1 ORDER BY appName COLLATE NOCASE")
    fun getActivePackages(): LiveData<List<Package>>

    @Query("SELECT COUNT(*) FROM package_table where timer = 1")
    fun getNumberOfTimers(): LiveData<Int>
}
