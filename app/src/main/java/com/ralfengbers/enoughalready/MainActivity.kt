package com.ralfengbers.enoughalready

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.media.AudioManager
import android.os.Bundle
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.widget.Toast
import android.widget.ToggleButton
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout


class MainActivity : AppCompatActivity() {

    private var viewModel: PackageViewModel? = null
    private var filterForActive: Boolean = false

    private val enabledNotificationListeners = "enabled_notification_listeners"

    private var packageAdapter: PackageAdapter? = null

    private var myAudioManager: AudioManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Set toolbar and app title
        findViewById<Toolbar>(R.id.toolbar).run {
            setSupportActionBar(this).apply {
                title = getString(R.string.app_name)
            }
        }

        // Initialize PackageManager and RecyclerView
        val pm = packageManager
        val packageRecyclerView = findViewById<RecyclerView>(R.id.package_list_view)

        packageAdapter = PackageAdapter(pm).apply {
            // Set the OnChanged Listener to the mute switch
            setOnMuteClickedListener(object : PackageAdapter.OnMuteClickedListener {
                override fun onMuteClicked(pkg: Package, isActive: Boolean) {
                    pkg.run {
                        isActivated = isActive
                        viewModel?.update(this)
                    }
                    if (!isActive) stopTimer(pkg)
                }

            })
        }

        packageRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = packageAdapter
            hasFixedSize()
        }

        // Initialize the ViewModel set the installed packages to the PackageAdapter
        viewModel = ViewModelProvider(this).get(PackageViewModel::class.java).apply {
            allPackages.observe(this@MainActivity, Observer<List<Package>> {
                Log.d(TAG, "onCreate | allPackages.observe")
                if (it != null && !filterForActive) packageAdapter?.setPackages(it)
            })
            activePackages.observe(this@MainActivity, Observer<List<Package>> {
                Log.d(TAG, "onCreate | activePackages.observe")
                if (it != null && filterForActive) packageAdapter?.setPackages(it)
            })
            numberOfTimers.observe(this@MainActivity, Observer<Int> {
                Log.d(TAG, "onCreate | numberOfTimers.observe")
                if (it == 0) myAudioManager?.ringerMode = viewModel?.ringerModeCache!!
            })
        }

        // Swipe to refresh container
        findViewById<SwipeRefreshLayout>(R.id.swipe_container).apply {
            setOnRefreshListener {
                viewModel?.updateInstalledPackages(pm)
                this.isRefreshing = false
            }
        }
    }

    private fun updateAdapter() {
        if (viewModel?.allPackages != null && viewModel?.activePackages != null) {
            when (filterForActive) {
                true -> packageAdapter?.setPackages(viewModel?.activePackages?.value!!)
                false -> packageAdapter?.setPackages(viewModel?.allPackages?.value!!)
            }
        }
    }

    /*
    Create options menu
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)

        val sharedPrefs: SharedPreferences = this.getSharedPreferences(
            Constants.SHARED_PREFERENCES,
            Context.MODE_PRIVATE
        )

        // App Activation Switch
        menu?.findItem(R.id.toolbar_switch)
            ?.actionView
            ?.findViewById<ToggleButton>(R.id.app_activation_toggle_button)
            ?.let {
                it.isChecked = sharedPrefs.getBoolean(Constants.APP_IS_ACTIVE, false)
                it.setOnCheckedChangeListener { _, isChecked ->
                    when (isChecked) {
                        true -> activateApp()
                        false -> deactivateApp()
                    }
                }
            }

        // Active Apps Filter Switch
        menu?.findItem(R.id.toolbar_switch)
            ?.actionView
            ?.findViewById<ToggleButton>(R.id.filter_active_toggle_button)
            ?.let {
                it.isChecked = sharedPrefs.getBoolean(Constants.APP_FILTER_IS_ACTIVE, false)
                it.setOnCheckedChangeListener { _, isChecked ->
                    filterForActive = isChecked
                    updateAdapter()
                }
            }
        return super.onCreateOptionsMenu(menu)
    }

    // Toast function
    private fun Context.toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun hasPermissions(context: Context, permissions: Array<out String>): Boolean {
        return permissions.all {
            ActivityCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
        }
    }

    /**
     * Is Notification Service Enabled.
     * Verifies if the notification listener service is enabled.
     * Got it from: https://github.com/kpbird/NotificationListenerService-Example/blob/master/NLSExample/src/main/java/com/kpbird/nlsexample/NLService.java
     * @return True if enabled, false otherwise.
     */
    private fun isNotificationServiceEnabled(): Boolean {
        Log.d(TAG, "isNotificationServiceEnabled")
        val pkgName = packageName
        val flat = Settings.Secure.getString(
            contentResolver,
            enabledNotificationListeners
        )
        if (!TextUtils.isEmpty(flat)) {
            val names = flat.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            for (i in names.indices) {
                ComponentName.unflattenFromString(names[i])?.let {
                    if (TextUtils.equals(pkgName, it.packageName)) {
                        return true
                    }
                }
            }
        }
        return false
    }

    /**
     * Build Notification Listener Alert Dialog.
     * Builds the alert dialog that pops up if the user has not turned
     * the Notification Listener Service on yet.
     * @return An alert dialog which leads to the notification enabling screen
     */
    private fun buildNotificationServiceAlertDialog(): AlertDialog {
        Log.d(TAG, "buildNotificationServiceAlertDialog")
        return AlertDialog.Builder(this).run {
            setTitle(R.string.notification_listener_service)
            setMessage(R.string.notification_listener_service_explanation)
            setPositiveButton(R.string.yes) { _, _ ->
                startActivity(Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS))
            }
            setNegativeButton(R.string.no) { _, _ ->
                toast("no")//app_activation_switch.isChecked = false
                // If you choose to not enable the notification listener
                // the app. will not work as expected
            }
            create()
        }
    }

    /*
    Activate the app
    Checks if NotificationListenerService is enabled and shows AlertDialog if not
    Registers receiver and gets the AudioManager
     */
    private fun activateApp() {
        Log.d(TAG, "activateApp")
        toast(getString(R.string.app_activated))
        if (!isNotificationServiceEnabled()) {
            buildNotificationServiceAlertDialog().show()
        }

        //TODO: there should be a unified way to request all permissions

        // The request code used in ActivityCompat.requestPermissions()
        // and returned in the Activity's onRequestPermissionsResult()
        val permissions: Array<out String> =
            arrayOf(android.Manifest.permission.READ_PHONE_STATE)

        if (!hasPermissions(this, permissions)) {
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_ALL)
        }

        this.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE)
            .edit().putBoolean(Constants.APP_IS_ACTIVE, true)
            .apply()

        // Finally we register a receiver to tell the MainActivity when a notification has been received
        // mReceiver = EANotificationBroadcastReceiver()
        //val intentFilter = IntentFilter()
        //intentFilter.addAction(getString(R.string.package_name))
        //registerReceiver(mReceiver, intentFilter)

        viewModel?.ringerModeCache = myAudioManager?.ringerMode
    }

    /*
    Deactivate App
    Unregisters the receiver
     */
    private fun deactivateApp() {
        Log.d(TAG, "deactivateApp")
        toast(getString(R.string.app_deactivated))
        //unregisterReceiver(mReceiver)
        myAudioManager?.ringerMode = viewModel?.ringerModeCache!!
        stopAllTimers()
        this.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE)
            .edit().putBoolean(Constants.APP_IS_ACTIVE, false)
            .apply()
    }

    private fun stopTimer(pkg: Package) {
        Log.d(TAG, "stopTimer: $pkg")
        Intent(Constants.NOTIFICATION_REMOVED).run {
            setPackage(getString(R.string.package_name))
            putExtra("packageName", pkg.packageName)
            sendBroadcast(this)
        }
    }

    private fun stopAllTimers() {
        viewModel?.activePackages?.value?.forEach {
            Intent(Constants.NOTIFICATION_REMOVED).run {
                setPackage(getString(R.string.package_name))
                putExtra("packageName", it.packageName)
                sendBroadcast(this)
            }
        }
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy")
        super.onDestroy()
    }

    companion object {
        const val TAG: String = "EA|MainActivity"
        const val PERMISSION_ALL = 1
    }
}