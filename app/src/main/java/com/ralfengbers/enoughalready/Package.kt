package com.ralfengbers.enoughalready

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Entity(tableName = "package_table")
@Parcelize
data class Package(
    @PrimaryKey val packageName: String,
    val appName: String,
    var isActivated: Boolean = false,
    var timer: Int = 0,
    var muteTime: Int = Constants.MUTE_TIME_DEFAULT
) : Parcelable