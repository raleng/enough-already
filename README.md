# Enough Already!

Ever had a friend who sends whole paragraphs, but one sentence and message at a time, so that your phone is ringing or vibrating every ten seconds for 2 minutes straight? Ever part of a group chat that explodes with messages the moment one person writes anything?

This app lets you controls the incoming notifications such that you get the initial notification to tell you something is happening, but mutes all other notifications for a specified time (default is 5 minutes) or until the notification is removed.