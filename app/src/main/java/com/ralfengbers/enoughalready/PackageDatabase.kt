package com.ralfengbers.enoughalready

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


// version relates to "migration strategy"
@Database(entities = [Package::class], version = 1)
abstract class PackageDatabase : RoomDatabase() {

    abstract fun packageDao(): PackageDao

    companion object {

        private var instance: PackageDatabase? = null

        @Synchronized
        fun getInstance(context: Context): PackageDatabase {
            return instance ?: Room.databaseBuilder(
                context.applicationContext,
                PackageDatabase::class.java, "package_database"
            )
                .fallbackToDestructiveMigration()
                .enableMultiInstanceInvalidation()
                .build()
        }

    }
}