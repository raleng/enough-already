package com.ralfengbers.enoughalready

import android.content.pm.PackageManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Switch
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class PackageAdapter(private val pm: PackageManager) :
    RecyclerView.Adapter<PackageAdapter.PackageHolder>() {

    private var packages: List<Package> = ArrayList()
    private var activeListener: OnMuteClickedListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): PackageHolder {
        return PackageHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.package_card, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return packages.size
    }

    override fun onBindViewHolder(holder: PackageHolder, position: Int) {
        val currentPackage = packages[position]
        holder.apply {
            iconView.setImageDrawable(pm.getApplicationIcon(currentPackage.packageName))
            appNameView.text = currentPackage.appName
            appMuteCountdownView.apply {
                visibility = if (currentPackage.timer == 0) View.INVISIBLE else View.VISIBLE
                text = (currentPackage.timer / 1000).toString()
            }
            activeAppSwitch.isChecked = currentPackage.isActivated
        }
    }

    fun setPackages(packages: List<Package>) {
        this.packages = packages
        notifyDataSetChanged()
    }

    inner class PackageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iconView: ImageView = itemView.findViewById(R.id.package_icon)
        val appNameView: TextView = itemView.findViewById(R.id.package_name)
        val appMuteCountdownView: TextView = itemView.findViewById(R.id.mute_countdown)
        val activeAppSwitch: Switch = itemView.findViewById(R.id.package_state)

        init {
            activeAppSwitch.setOnClickListener {
                val isActive = activeAppSwitch.isChecked
                val position = adapterPosition
                Log.d("Mute Listener: ", "Switch changed")
                if (activeListener != null && position != RecyclerView.NO_POSITION) {
                    activeListener?.onMuteClicked(packages[position], isActive)
                }
            }
        }
    }

    interface OnMuteClickedListener {
        fun onMuteClicked(pkg: Package, isActive: Boolean)
    }

    fun setOnMuteClickedListener(listener: OnMuteClickedListener) {
        this.activeListener = listener
    }

}